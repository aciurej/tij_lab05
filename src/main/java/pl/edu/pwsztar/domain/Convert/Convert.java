package pl.edu.pwsztar.domain.Convert;

public interface Convert<MD,M>{
    MD convert(M from);
}


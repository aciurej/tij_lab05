package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.Convert.Convert;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;


@Component
public class MovieMapper implements Convert<Movie,CreateMovieDto> {

    @Override
    public Movie convert(CreateMovieDto from) {
        Movie movie = new Movie();
        movie.setTitle(from.getTitle());
        movie.setImage(from.getImage());
        movie.setYear(from.getYear());
        return movie;

    }
}

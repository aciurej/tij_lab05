package pl.edu.pwsztar.service.exceptions;

public class MovieNotExistException extends Exception{
    public MovieNotExistException(String message){
        super(message);
    }
    public MovieNotExistException(){
        super();
    }
}
